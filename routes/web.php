<?php

use App\Http\Controllers\PdfController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  [PdfController::class, 'index']);
Route::post('store-form', [PdfController::class, 'store'])->name('store-form');
Route::get('certificate/{certificate_id}', [PdfController::class, 'certificate'])->name('certificate');
Route::get('/{file}', [PdfController::class, 'downloadFilePDF'])->name('downloadFilePDF');
