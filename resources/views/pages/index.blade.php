@extends('layout.layout')

@section('content')
    <div class="container mt-4">
        @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="card">
            <div class="card-header text-center font-weight-bold">
                Certificates
            </div>
            <div class="card-body">
                <form name="certificates-form" id="certificates-form" method="post" action="{{route('store-form')}}">
                    @csrf
                    <div class="form-group">
                        <label for="certificate_number">Certificate number</label>
                        <input type="text" id="certificate_number" name="certificate_number" class="form-control"
                               value="{{ old('certificate_number') ?? '' }}">
                        @if( $errors->first('certificate_number') !== '')
                            <div class="alert alert-danger" role="alert">{{ $errors->first('certificate_number') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="course_name">Course name</label>
                        <input type="text" id="course_name" name="course_name" class="form-control"
                               value="{{ old('course_name') ?? '' }}">
                        @if( $errors->first('course_name') !== '')
                            <div class="alert alert-danger" role="alert">{{ $errors->first('course_name') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="student_name">Student name</label>
                        <input type="text" id="student_name" name="student_name" class="form-control"
                               value="{{ old('student_name') ?? '' }}">
                        @if( $errors->first('student_name') !== '')
                            <div class="alert alert-danger" role="alert">{{ $errors->first('student_name') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="course_end_date">Course end date</label>
                        <input type="date" id="course_end_date" name="course_end_date" class="form-control"
                               value="{{ old('course_end_date') ?? '' }}">
                        @if( $errors->first('course_end_date') !== '')
                            <div class="alert alert-danger" role="alert">{{ $errors->first('course_end_date') }}</div>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary">Get certificate</button>
                </form>
            </div>
        </div>
    </div>

@endsection
