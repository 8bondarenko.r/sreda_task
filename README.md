```
composer install
create .env file
php artisan migrate
php artisan storage:link
php artisan key:generate
```
### Structure
```
app
    Http
        Controllers    
                PdfController.php
    Service
       CreateFolders.php
       CreatePDFCertificate.php
       CreateQRCode.php
                    
```
### route file
```
routes
    web.php
```
