<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificatesPdfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificates_pdf', function (Blueprint $table) {
            $table->id();
            $table->string('certificate_pdf_file_name');

            $table->unsignedBigInteger('qr_code_id');

            $table->foreign('qr_code_id')
                ->references('id')
                ->on('qr_codes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates_pdf');
    }
}
