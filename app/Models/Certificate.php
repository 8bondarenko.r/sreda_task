<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Certificate
 * @package App\Models
 */
class Certificate extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'certificate_number',
        'course_name',
        'student_name',
        'course_end_date',
    ];

    /**
     * @param array $data
     * @return mixed
     */
    static public function storeCertificate(array $data)
    {
        return self::create($data);
    }

    /**
     * @return HasOne
     */
    public function qrCode(): HasOne
    {
        return $this->hasOne(QRCodes::class);
    }
}
