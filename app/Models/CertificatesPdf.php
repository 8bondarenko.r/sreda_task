<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class CertificatesPdf
 * @package App\Models
 */
class CertificatesPdf extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'certificate_pdf_file_name',
        'qr_code_id',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'certificates_pdf';

    /**
     * @param array $data
     * @return mixed
     */
    static public function storeCertificatePDF(array $data)
    {
        return self::create($data);
    }

    /**
     * @return BelongsTo
     */
    public function qrCode(): BelongsTo
    {
        return $this->belongsTo(QRCodes::class, 'qr_code_id');
    }
}
