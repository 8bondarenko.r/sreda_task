<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class QRCodes
 * @package App\Models
 */
class QRCodes extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'qr_code_file_name',
        'certificate_id',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'qr_codes';

    /**
     * @param array $data
     * @return mixed
     */
    static public function storeQRCode(array $data)
    {
        return self::create($data);
    }

    /**
     * @return HasOne
     */
    public function certificatePDF(): HasOne
    {
        return $this->hasOne(CertificatesPdf::class, 'qr_code_id');
    }

    /**
     * @return BelongsTo
     */
    public function qrCode(): BelongsTo
    {
        return $this->belongsTo(Certificate::class);
    }
}
