<?php


namespace App\Http\Controllers;

use App\Http\Requests\StoreCertificateFormRequest;
use App\Http\Service\CreatePDFCertificate;
use App\Http\Service\CreateQRCode;
use App\Models\Certificate;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\{Factory, View};
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Symfony\Component\HttpFoundation\BinaryFileResponse;


/**
 * Class PdfController
 * @package App\Http\Controllers
 */
class PdfController extends Controller
{
    /**
     * @var CreatePDFCertificate
     */
    public $servicePDFCertificate;

    /**
     * @var CreateQRCode
     */
    public $serviceQRCode;

    /**
     * PdfController constructor.
     * @param CreatePDFCertificate $servicePDFCertificate
     * @param CreateQRCode $serviceQRCode
     */
    public function __construct(CreatePDFCertificate $servicePDFCertificate, CreateQRCode $serviceQRCode)
    {
        $this->servicePDFCertificate = $servicePDFCertificate;
        $this->serviceQRCode = $serviceQRCode;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('pages.index');
    }

    /**
     * @param StoreCertificateFormRequest $request
     * @return Application|RedirectResponse|Redirector|string
     */
    public function store(StoreCertificateFormRequest $request)
    {
        try {
            $dataCertificate = Certificate::storeCertificate($request->post());
            $dataQRCode = $this->serviceQRCode->generateQRCode($dataCertificate->getAttributes());
            $this->servicePDFCertificate->createDPFCertificate(
                $dataCertificate->getAttributes(),
                $dataQRCode->getAttributes(),
            );

            return redirect(route('certificate', ['certificate_id' => $dataCertificate['id']]));

        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function certificate($id)
    {
        $certificateData = Certificate::find($id);

        return view('pages.certificate_page', [
            'certificateData' => $certificateData,
            'qr_code' => $certificateData->qrCode,
            'pdf_file' => $certificateData->qrCode->certificatePDF
        ]);
    }

    /**
     * @param string $fileName
     * @return BinaryFileResponse
     */
    public function downloadFilePDF(string $fileName): BinaryFileResponse
    {
        return response()->download('storage/pdf/' . $fileName);
    }
}
