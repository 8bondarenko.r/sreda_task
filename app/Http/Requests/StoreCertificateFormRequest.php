<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCertificateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'certificate_number' => 'required',
            'course_name' => 'required',
            'student_name' => 'required',
            'course_end_date' => 'required|date',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'certificate_number.required' => 'A certificate number is required',
            'course_name.required' => 'A course name is required',
            'student_name.required' => 'A student name is required',
            'course_end_date.required' => 'A course end date is required',
        ];
    }
}
