<?php


namespace App\Http\Service;

use App\Models\QRCodes;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/**
 * Class CreateQRCode
 * @package App\Http\Service
 */
class CreateQRCode
{
    /**
     * @param array $data
     * @return mixed
     */
    static public function generateQRCode(array $data): QRCodes
    {
        $pathToQRCode = storage_path() . '/app/public/qr_code/' . $data['certificate_number'] . '.svg';

        QrCode::generate(route('certificate', ['certificate_id' => $data['id']]), $pathToQRCode);

        return QRCodes::storeQRCode([
            'qr_code_file_name' => $data['certificate_number'] . '.svg',
            'certificate_id' => $data['id'],
        ]);
    }
}
