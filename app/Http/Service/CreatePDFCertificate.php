<?php


namespace App\Http\Service;

use App\Models\CertificatesPdf;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Str;

/**
 * Class CreatePDFCertificate
 * @package App\Http\Service
 */
class CreatePDFCertificate
{
    /**
     * @param array $dataCertificate
     * @param array $dataQRCode
     * @return array
     */
    public function createDPFCertificate(array $dataCertificate, array $dataQRCode): array
    {
        CreateFolders::createFolders('public/pdf');

        $nameFile = $dataCertificate['certificate_number'] . time() . Str::random(10) . '.pdf';

        $pathToFile = storage_path() . '/app/public/pdf/';
        $pdf = PDF::loadView('pages.pdf_file', [
            'dataCertificate' => $dataCertificate,
            'dataQRCode' => $dataQRCode['qr_code_file_name']
        ])->save($pathToFile . $nameFile);

        $savedPathPdf = CertificatesPdf::storeCertificatePDF([
            'certificate_pdf_file_name' => $nameFile,
            'qr_code_id' => $dataQRCode['id'],
        ]);

        return [
            'pdf_data' => $pdf->download($nameFile),
            'savedPathPdf' => $savedPathPdf,
        ];
    }

}
