<?php


namespace App\Http\Service;


use Illuminate\Support\Facades\Storage;

class CreateFolders
{
    /**
     * @param string $folder
     */
    static public function createFolders(string $folder = ''): void
    {
        $file_path = Storage::path($folder);

        if (!is_dir($file_path)) {
            mkdir($file_path, 0775, true);
        }
    }
}
